//soal 1
console.log("Soal 1")
function string_sort(str) {
    var i = 0, j;
    while (i < str.length) {
        j = i + 1;
        while (j < str.length) {
            if (str[j] < str[i]) {
                var temp = str[i];
                str[i] = str[j];
                str[j] = temp;
            }
            j++;
        }
        i++;

    }

}

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]
string_sort(daftarHewan)
console.log(daftarHewan)

console.log("\n")

//pembahasan
console.log("Pembahsan soal 1")
var daftarBinatang = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]
var binatang = daftarBinatang.sort()
for (var i = 0; i < binatang.length; i++) {

    console.log(binatang[i])
}

console.log("\n")

//soal 2
console.log("soal 2")
function introduce(array) {
    return "Nama Saya " + array.name + ", umur saya " + array.age + " tahun, alamat saya di " + array.address + ", dan saya punya hobby yaitu " + array.hobby
}

var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" }

var perkenalan = introduce(data)
console.log(perkenalan)
// Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

console.log("\n")

//soal 3
console.log("Soal 3")
function hitung_huruf_vokal(str1) {
    const count = str1.match(/[aeiou]/gi).length;
    return count;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1, hitung_2) // 3 2

console.log("\n")

//soal 4
console.log("Soal 4")
function hitung(n) {
    return 2 * n - 2
}

console.log(hitung(0)) // -2
console.log(hitung(1)) // 0
console.log(hitung(2)) // 2
console.log(hitung(3)) // 4
console.log(hitung(5)) // 8


