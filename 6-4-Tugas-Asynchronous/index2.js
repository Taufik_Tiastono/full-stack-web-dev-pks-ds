var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
]

// Lanjutkan code untuk menjalankan function readBooksPromise
var booksAntrian = books.length;
var time = 10000;

function execute(time, ind, booksAntrian) {
    readBooksPromise(time, books[ind])
        .then(function (remainingTime) {
            time = remainingTime;
            booksAntrian = booksAntrian - 1;
            if (booksAntrian > 0) {
                execute(time, ind + 1, booksAntrian);
            }
        })
        .catch(function (error) {

        })
}

execute(time, 0, booksAntrian);