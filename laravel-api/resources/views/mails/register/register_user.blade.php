<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
</head>
<body>
    <div class="container ">
        <div class="d-flex justify-content-center">
            <div class="card text-center">
                <div class="card-header">
                   
                </div>
                <div class="card-body">
                  <h5 class="card-title">Selamat datang di DuniaMaya</h5>
                  <p class="card-text">ini adalah kode OTP Anda :  {{$otp_code->otp}}  Kode OTP ini berlaku 5 menit. Jangan berikan kode ini kepada siapapun."</p>
                  {{-- <a href="#" class="btn btn-primary">Go somewhere</a> --}}
                  <h3><span class="badge badge-info">{{$otp_code->otp}}</span></h3>
                </div>
                <div class="card-footer text-muted">
                    Full Stack Web Dev
                </div>
              </div>
        </div>
    </div>
</body>
</html>