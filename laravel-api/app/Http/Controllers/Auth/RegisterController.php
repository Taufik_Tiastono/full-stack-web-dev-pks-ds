<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\OtpCode;
use App\User;
use Carbon\Carbon;
//use Illuminate\Support\Facades\Mail;
//use App\Mail\RegisterUser;
use App\Events\RegisterStoredEvent;
//use App\Roles;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //test
        //dd('masuk');

        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'name' => 'required',
            'email' => 'required|unique:users,email|email',
            'username' => 'required|unique:users,username'
            //'role_id' => 'required'
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }


        $user = User::create($allRequest);

        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $random)->first();

        } while ($check);

        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(5),
            'user_id' => $user->id
        ]);


        //KIRIM email otp code ke email register
        //Mail::to($otp_code->user->email)->send(new RegisterUser($otp_code));

        //panggil event RegisterStoredEvent
        event(new RegisterStoredEvent($otp_code));

        return response()->json([
            'success' => true,
            'message' => 'Data User Berhasil dibuat',
            'data' => [
                'users' => $user,
                'otp_codes' => $otp_code
            ]
        ]);


    }
}
