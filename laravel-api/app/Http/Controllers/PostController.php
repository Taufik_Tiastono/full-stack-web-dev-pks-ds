<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Validator;
use App\User;

class PostController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store', 'update', 'destroy']);
    }
    
    //
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
         //get data from table posts
        $posts = Post::latest()->get();
 
        //test
        //dd($posts);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Post',
            'data' => $posts
        ], 200);


    }

    /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
         //find post by ID
        $post = Post::find($id);

        if ($post) {   
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Data Post',
                'data' => $post
            ], 200);
        }


        return response()->json([
            'success' => false,
            'message' => 'Data dengan id: ' . $id . ' tidak ditemukan',
        ], 404);




    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
         //set validation
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //menmabahkan user_id
        $user = auth()->user();
 
         //save to database
        $post = Post::create([
            'title' => $request->title,
            'description' => $request->description,
            'user_id' => $user->id,
        ]);
 
         //success save to database
        if ($post) {

            return response()->json([
                'success' => true,
                'message' => 'Post Created',
                'data' => $post
            ], 201);

        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Post Failed to Save',
        ], 409);

    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, $id)
    {

        $allRequest = $request->all();

         //set validation
        $validator = Validator::make($allRequest, [
            'title' => 'required',
            'description' => 'required',
        ]);
         
         //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
 
         //find post by ID
        $post = Post::find($id);



        if ($post) {

            $user = auth()->user();

            //cek user_id sama atau tidak
            if ($post->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',
                ], 403);
            }

             //update post
            $post->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Post with title ' . $post->title . ' Updated',
                'data' => $post
            ], 200);

        }
 
         //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Data with ' . $id . '  Not Found',
        ], 404);

    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
         //find post by ID
        $post = Post::find($id);

        if ($post) {

            $user = auth()->user();

            //cek user_id sama atau tidak
            if ($post->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',
                ], 403);
            }
             //delete post
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post Deleted',
            ], 200);

        }
 
         //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);
    }
}
