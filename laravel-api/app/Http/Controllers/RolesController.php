<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Roles;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $roles = Roles::latest()->get();
 
        //test
        //dd($roless);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Roles',
            'data' => $roles
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required'

        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
 
         //save to database
        $roles = Roles::create([
            'name' => $request->name
        ]);
 
         //success save to database
        if ($roles) {

            return response()->json([
                'success' => true,
                'message' => 'Roles Created',
                'data' => $roles
            ], 201);

        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Roles Failed to Save',
        ], 409);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $roles = Roles::find($id);

        if ($roles) {   
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Data Roles',
                'data' => $roles
            ], 200);
        }


        return response()->json([
            'success' => false,
            'message' => 'Data dengan id: ' . $id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $allRequest = $request->all();

         //set validation
        $validator = Validator::make($allRequest, [
            'name' => 'required',
        ]);
         
         //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
 
         //find Roles by ID
        $roles = Roles::find($id);

        if ($roles) {
 
             //update Roles
            $roles->update([
                'name' => $request->name
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Roles with title ' . $roles->name . ' Updated',
                'data' => $roles
            ], 200);

        }
 
         //data Roles not found
        return response()->json([
            'success' => false,
            'message' => 'Data with ' . $id . '  Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $roles = Roles::find($id);

        if ($roles) {
 
             //delete Roles
            $roles->delete();

            return response()->json([
                'success' => true,
                'message' => 'Roles Deleted',
            ], 200);

        }
 
         //data Roles not found
        return response()->json([
            'success' => false,
            'message' => 'Roles Not Found',
        ], 404);
    }
}
