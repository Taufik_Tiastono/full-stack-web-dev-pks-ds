<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comments;
use Illuminate\Support\Facades\Validator;
use App\User;
//use Illuminate\Support\Facades\Mail;
//use App\Mail\PostAuthorMail;
use App\Post;
//use App\Mail\CommentAuthorMail;
use App\Events\CommentStoredEvent;

class CommentsController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth:api')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //tampilkan comment berdasarakn post_id
        $post_id = $request->post_id;
        //
        //get data from table comments
        $comment = Comments::where('post_id', $post_id)->latest()->get();
 
        //test
        //dd($posts);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Comments',
            'data' => $comment
        ], 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //set validation
        $validator = Validator::make($request->all(), [
            'content' => 'required',
            'post_id' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }


        $user = auth()->user();

         //save to database
        $comment = Comments::create([
            'content' => $request->content,
            'post_id' => $request->post_id,
            'user_id' => $user->id,
        ]);

        //ini dikirim pada yg memiliki post
        //Mail::to($comment->post->user->email)->send(new PostAuthorMail($comment));
        
        //ini dikirim pada yg memiliki komen
        //Mail::to($comment->user->email)->send(new CommentAuthorMail($comment));
 
        
        //Memanggil Event untuk kirim emaail
        event(new CommentStoredEvent($comment));
        
         //success save to database
        if ($comment) {

            return response()->json([
                'success' => true,
                'message' => 'Comment Created',
                'data' => $comment
            ], 201);

        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Comment Failed to Save',
        ], 409);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        //find post by ID
        $comment = Comments::find($id);

        if ($comment) {   
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Comment',
                'data' => $comment
            ], 200);
        }


        return response()->json([
            'success' => false,
            'message' => 'Comment dengan id: ' . $id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $allRequest = $request->all();

        //set validation
        $validator = Validator::make($allRequest, [
            'content' => 'required',

        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $comment = Comments::find($id);

        if ($comment) {

            $user = auth()->user();

            //cek user_id sama atau tidak
            if ($comment->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data comment bukan milik user login',
                ], 403);
            }

            //update post
            $comment->update([
                'content' => $request->content,

            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment Updated',
                'data' => $comment
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Data with ' . $id . '  Not Found',
        ], 404);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $comment = Comments::find($id);

        if ($comment) {

            $user = auth()->user();

            //cek user_id sama atau tidak
            if ($comment->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data comment bukan milik user login',
                ], 403);
            }
            
             //delete post
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted',
            ], 200);

        }
 
         //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }
}
