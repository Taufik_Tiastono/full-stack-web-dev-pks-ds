<?php

namespace App\Listeners;

use App\Events\RegenerateOtpCodeStoredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegenerateOtpCode;


class SendOtpCodetoUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegenerateOtpCodeStoredEvent  $event
     * @return void
     */
    public function handle(RegenerateOtpCodeStoredEvent $event)
    {
        //
        Mail::to($event->otp_code->user->email)->send(new RegenerateOtpCode($event->otp_code));
    }
}
