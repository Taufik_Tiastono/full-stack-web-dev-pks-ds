<?php

namespace App\Listeners;

use App\Events\RegisterStoredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\RegisterUser;
use Illuminate\Support\Facades\Mail;

class SendEmailtoUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterStoredEvent  $event
     * @return void
     */
    public function handle(RegisterStoredEvent $event)
    {
        //
        Mail::to($event->otp_code->user->email)->send(new RegisterUser($event->otp_code));
    }
}
