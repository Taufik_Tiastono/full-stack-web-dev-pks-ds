<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Post extends Model
{
    //

    protected $table = 'posts';
    protected $fillable = ['title', 'description', 'user_id'];

    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }

            //menambahkan user_id bisa menggunakan ini atau yg ada di Controller
            //$model->user_id = auth()->user()->id;
        });
    }

    public function comments()
    {
        return $this->hasMany('App\Comments');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
