<?php 
abstract class Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function __construct($nama) {
      $this->nama = $nama;
      //$this->darah = $darah;
      //$this->jumlahKaki = $jumlahKaki;
      //$this->keahlian = $keahlian;
      //echo $this->nama ." sedang ".$this->keahlian;

    }
    
    // public function __destruct(){
    //   echo "object ".$this->nama." dihapus";
    // }

    public function atraksi(){
      return $this->nama." sedang ".$this->keahlian;
    }
     

}

abstract class Fight {
    public $attackPower;
    public $defencePower;

    public function __construct($attackPower,$defencePower) {
      $this->attackPower = $attackPower;
      $this->defencePower = $defencePower;

    }


     public function diserang(){

     }

     public function serang() {

     }
    
}


class Harimau extends Hewan {
  public $jumlahKaki = 4;
  public $keahlian = "lari cepat";
}


class Elang extends Hewan {
  public $jumlahKaki = 2;
  public $keahlian = "terbang tinggi";

}


//$harimau1 = new Harimau("harimau maung", "berlari kencang");
echo "<br>";

//$harimau2 = new Harimau("Harimau Buas");
//$harimau2 = new Harimau("Harimau Buas","Lari Cepat");

$harimau2 = new Harimau("Harimau Buas");
echo $harimau2->atraksi();

echo "<br><br>";

$elang2 = new Elang("Elang2");
echo $elang2->atraksi();


//mamanggil function destruct()
// echo "<br><br>";
// unset($harimau1);


?>