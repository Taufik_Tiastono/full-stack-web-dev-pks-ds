export const FormComponent = {
    template: `
<form @submit.prevent="$emit('submit-form')">
    <p v-if="kesalahan.length">
        <b>Please correct the following errors :</b>
        <ul>
            <li v-for="error in kesalahan">{{ error }}</li>
        </ul>
    </p>
    <label>Name : </label>
    <input type="text" :disabled="buttonstatus=='upload'" name="name" ref="name" v-model='nama' @input="$emit('input-name',nama)">
    <label>Address : </label>
    <textarea :disabled="buttonstatus=='upload'" name="address" ref="address"  v-model='jalan' @input="$emit('input-address',jalan)"></textarea>
    <br>
    <label>No Hp : </label>
    <input type="number" :disabled="buttonstatus=='upload'" name="no_hp" ref="no_hp" v-model='no' @input="$emit('input-nohp', no)">
    <br>
    <br>
    <div v-if="buttonstatus == 'submit'">
        <input type="submit" value="SUBMIT">
    </div>
    <div v-else-if="buttonstatus == 'update'">
        <input type="button" @click="$emit('update-member', ideditmember)" value="UPDATE">
        <input type="button" value="CANCEL" @click="$emit('clear-form', bool)" style="margin-top:5px">
    </div>
    <div v-else-if="buttonstatus == 'upload'">
        <input type="file" name="photo_profile" ref="photo_profile">
        <input type="button" value="UPLOAD" @click="$emit('submit-photo', idupload)" style="margin-top: 5px">
        <input type="button" value="CANCEL" @click="$emit('clear-form', bool)" style="margin-top:5px">
    </div>
</form>
`,
    props: ['kesalahan', 'name', 'no_hp', 'address', 'buttonstatus', 'ideditmember', 'idupload'],
    watch: {
        name: function (newVal, oldVal) {
            this.nama = newVal
        },
        address: function (newVal, oldVal) {
            this.jalan = newVal
        },
        no_hp: function (newVal, oldVal) {
            this.no = newVal
        },
    },
    data() {
        return {
            bool: true,
            nama: '',
            jalan: '',
            no: ''
        }
    }
}
