export const TableComponent = {
    template: `
    <table border=1>
    <tr>
        <td>
            <b>Foto</b>
        </td>
        <td>
            <b>Name</b>
        </td>
        <td>
            <b>Address</b>
        </td>
        <td>
            <b>No HP</b>
        </td>
        <td>
            <b>Action</b>
        </td>
    </tr>
    <tr v-for="member of members">
        <!-- <td>
            {{member.id}}
        </td> -->
        <td>
            <img width=100 :src="member.photo_profile ? photodomain + member.photo_profile : 'https://dummyimage.com/16:9x1080'" alt="">
        </td>
        <td>
            {{member.name}}
        </td>
        <td>
            {{member.address}}
        </td>
        <td>
            {{member.no_hp}}
        </td>
        <td>
            <button @click="$emit('edit-member',member)">Edit</button>
            <button @click="$emit('remove-member',member.id)">Hapus</button>
            <button @click="$emit('upload-photo',member)">Upload Foto</button>
        </td>
    </tr>
</table>
    `,
    props: ['members', 'photodomain'],
}
