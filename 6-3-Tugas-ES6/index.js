console.log("soal 1")

const PersegiPanjang = (p, l) => {
    return {
        p, l,
        luas() {
            return p * l
        },
        keliling() {
            return 2 * (p + l)
        }
    }
}

//console.log(PersegiPanjang(10, 5).luas())
let luasPP = PersegiPanjang(10, 5).luas()
console.log(`Luas Persegi Panjang : ${luasPP}`)

let kelilingPP = PersegiPanjang(10, 5).keliling()
console.log(`Keliling Persegi Panjang : ${kelilingPP}`)

// const kelilingPersegiPanjang = (p, l) => {
//     return 2 * (p + l)
// }

// let keliling = kelilingPersegiPanjang(10, 5)
// console.log("Keliling Persegi Panjang : " + keliling)

console.log("\n")


console.log("soal 2")


const newFunction = (firstName, lastName) => {
    return {
        firstName, lastName,
        fullName() {
            console.log(`${firstName} ${lastName}`)
        }
    }
}
//Driver Code 
newFunction("William", "Imoh").fullName()

console.log("\n")


console.log("soal 3")
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const { firstName, lastName, address, hobby } = newObject
console.log(firstName, lastName, address, hobby)

console.log("\n")


console.log("soal 4")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//const combined = west.concat(east)
const combined = [...west, ...east]
//Driver Code
console.log(combined)
console.log("\n")


console.log("soal 5")

const planet = "earth"
const view = "glass"
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(before)

console.log("\n")

//testCobacoba
// function getTriangle(base, height) {
//     return {
//         base,
//         height,
//         area() {
//             return (base * height) / 2
//         },
//         asal() {
//             return base * height
//         }
//     }
// }

// console.log(getTriangle(3, 5).area())
// console.log(getTriangle(3, 5).asal())